#!/usr/bin/env python
"""
clip_lib.common - Common utility functions and base classes for
                  clip-related extensions

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
import re
import sys
import locale

# compat
# import six

# local library
import inkex
import simpletransform
# import cubicsuperpath
import clip_lib.transform as mat


try:
    inkex.localize()
except AttributeError:
    import gettext  # pylint: disable=wrong-import-order
    _ = gettext.gettext


# Constants
SVG_SHAPES = ('rect', 'circle', 'ellipse', 'line', 'polyline', 'polygon')
IGNORE_ELEMENTS = [
    inkex.addNS('defs', 'svg'),
    inkex.addNS('style', 'svg'),
    inkex.addNS('script', 'svg'),
    inkex.addNS('desc', 'svg'),
    inkex.addNS('title', 'svg'),
    inkex.addNS('metadata', 'svg'),
    inkex.addNS('namedview', 'sodipodi')
]

ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()


# Utiliy functions for selection checking

def zSort(inNode, idList):
    """Z-sort selected objects, return list of IDs."""
    # pylint: disable=invalid-name
    # TODO: include function in shared module simpletransform.py
    sortedList = []
    theid = inNode.get("id")
    if theid in idList:
        sortedList.append(theid)
    for child in inNode:
        if len(sortedList) == len(idList):
            break
        sortedList += zSort(child, idList)
    return sortedList


def is_clipped(node):
    """Return value of clip-path attribute (fallback: None)."""
    clip_path = node.get('clip-path')
    return clip_path != 'none' and clip_path is not None


def is_clip_path(node):
    """Check node for clipPath tag."""
    return node.tag == inkex.addNS('clipPath', 'svg')


def is_rect(node):
    """Check whether node is SVG rect element."""
    return node.tag == inkex.addNS('rect', 'svg')


def is_group(node):
    """Check whether node is SVG group element."""
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    """Check whether node is Inkscape layer."""
    return (node.tag == inkex.addNS('g', 'svg') and
            node.get(inkex.addNS('groupmode', 'inkscape')) == 'layer')


def layers(document, rev=False):
    """Iterate through layers in document, return label and node."""
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            label = node.get(inkex.addNS('label', 'inkscape'), None)
            if sys.version_info < (3,):
                labelstring = label.encode(ENCODING)
            else:
                labelstring = label
            yield (labelstring, node)


def layer_objs(document, rev=False):
    """Iterate through layers in document, return layer node."""
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def sublayer_objs(layer, rev=False):
    """Iterate through sublayers of layer, return sub-layer node."""
    for node in layer.iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def get_toplevel_layer(node):
    """Return parent top-level layer of node."""
    if is_layer(node.getparent()):
        node = get_toplevel_layer(node.getparent())
    return node


def get_parent_layer(node):
    """Return parent layer of node."""
    if not is_layer(node.getparent()):
        node = get_parent_layer(node.getparent())
    return node.getparent()


def wrap_group(node):
    """Wrap node in group, return group."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    index = node.getparent().index(node)
    node.getparent().insert(index, group)
    group.append(node)
    return group


def group_with(node, path):
    """Put path into group wrapped around node, return group."""
    group = wrap_group(node)
    if group is not None:
        mat.apply_absolute_diff(node, path)
        group.append(path)
    return group


class EffectCompat(inkex.Effect):
    """Effect class providing compat methods for document scale."""

    def __init__(self):
        """Init inkex.Effect class, parse default options."""
        inkex.Effect.__init__(self)

        # support legacy documents (allow dpi override)
        self.OptionParser.add_option("--override_dpi",
                                     action="store",
                                     type="string",
                                     dest="override_dpi",
                                     default="pass",
                                     help="Override internal dpi")

    # -----------------------------------------------------------------
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    #

    # Maintain separate dicts for 90 and 96 dpi to allow extensions to
    # implement support for legacy (or future) documents.
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.543307086614174,
        'cm': 35.43307086614174,
        'm':  3543.307086614174,
        'km': 3543307.086614174,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0
    }

    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.333333333333333,
        'px': 1.0,
        'mm': 3.779527559055119,
        'cm': 37.79527559055119,
        'm':  3779.527559055119,
        'km': 3779527.559055119,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0
    }

    # A dictionary of unit to user unit conversion factors.
    __uuconv = __uuconv_96dpi

    # New method: get_document_scale()
    # ================================
    # The new method supports arbitrary uniform scale factors and is
    # used in the unit conversion methods unittouu() and uutounit().

    def match_uuconv(self, val, eps=0.01):
        """Fuzzy matching of value to one of the known unit factors."""
        match = None
        for key in self.__uuconv:
            if inkex.are_near_relative(self.__uuconv[key], val, eps):
                match = self.__uuconv[key]
        return match or val

    def split_svg_length(self, string, percent=False):
        """Split SVG length string into float and unit string."""
        # pylint: disable=invalid-name
        param = unit = None
        if percent:
            unit_list = '|'.join(list(self.__uuconv.keys()) + ['%'])
        else:
            unit_list = '|'.join(list(self.__uuconv.keys()))
        param_re = re.compile(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_re = re.compile(
            '({0})$'.format(unit_list))
        if string is not None:
            p = param_re.match(string)
            u = unit_re.search(string)
            if p:
                try:
                    param = float(p.string[p.start():p.end()])
                except (KeyError, ValueError):
                    pass
            if u:
                try:
                    unit = u.string[u.start():u.end()]
                except KeyError:
                    pass
        return (param, unit)

    def get_page_dimension(self, attribute):
        """Retrieve value for SVGRoot attribute passed as argument.

        Return list of float and string.
        """
        string = self.document.getroot().get(attribute, "100%")
        val, unit = self.split_svg_length(string, percent=True)
        dimension = 100.0 if val is None else val
        dimension_unit = 'px' if unit is None else unit
        return (dimension, dimension_unit)

    def get_viewbox(self):
        """Retrieve value for viewBox from current document.

        Return list of 4 floats or None.
        """
        viewbox_attribute = self.document.getroot().get('viewBox', None)
        viewbox = []
        if viewbox_attribute:
            try:
                viewbox = list(float(i) for i in viewbox_attribute.split())
            except ValueError:
                pass
        if len(viewbox) != 4 or (viewbox[2] < 0 or viewbox[3] < 0):
            viewbox = None
        return viewbox

    def check_viewbox(self, width, w_unit, height, h_unit):
        """Verify values for SVGRoot viewBox width and height.

        Return list of 2 floats.
        """
        viewbox = self.get_viewbox()
        if viewbox is None:
            # If viewBox attribute is missing or invalid: calculate
            # viewBox dimensions corresponding to width, height
            # attributes. Treat '%' as special case, else assume 96dpi.
            vb_width = (width if w_unit == '%' else
                        self.__uuconv[w_unit] * width)
            vb_height = (height if h_unit == '%' else
                         self.__uuconv[h_unit] * height)
        else:
            vb_width, vb_height = viewbox[2:4]
        return (vb_width, vb_height)

    def get_aspectratio(self):
        """Get preserveAspectRatio from SVGRoot.

        Return list of 2 strings.
        """
        preserve_aspect_ratio = self.document.getroot().get(
            'preserveAspectRatio', "xMidYMid meet")
        try:
            return preserve_aspect_ratio.split()
        except ValueError:
            # Nothing to split: Set <meetOrSlice> parameter to default
            # 'meet'.  Note that the <meetOrSlice> parameter is ignored
            # if <align> parameter is 'none'.
            return (preserve_aspect_ratio, "meet")

    def get_aspectratio_scale(self, width, w_unit, height, h_unit,
                              vb_width, vb_height):
        """Calculate offset, scale based on SVGRoot preserveAspectRatio.

        Return list of 4 floats.
        """
        # pylint: disable=too-many-arguments
        # pylint: disable=too-many-locals

        x_offset = y_offset = 0.0
        scale = 1.0
        aspect_align, aspect_clip = self.get_aspectratio()
        if aspect_align == 'none':
            # TODO: implement support for non-uniform scaling
            # based on preserveAspectRatio attribute.
            pass
        else:
            width = vb_width * (width / 100.0) if w_unit == '%' else width
            height = vb_height * (height / 100.0) if h_unit == '%' else height
            vb_width = width if vb_width == 0 else vb_width
            vb_height = height if vb_width == 0 else vb_height
            scale_x = width / vb_width
            scale_y = height / vb_height
            # Force uniform scaling based on <meetOrSlice> parameter
            scale = (aspect_clip == "meet" and
                     min(scale_x, scale_y) or
                     max(scale_x, scale_y))
            # calculate offset based on <align> parameter
            align_x = aspect_align[1:4]
            align_y = aspect_align[5:8]
            offset_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
            try:
                # TODO: verify units of calculated offsets
                x_offset = round(
                    offset_factor[align_x] * (width - vb_width*scale), 3)
                y_offset = round(
                    offset_factor[align_y] * (height - vb_height*scale), 3)
            except KeyError:
                pass
        return (x_offset, y_offset, scale, scale)

    def apply_viewbox(self):
        """Return offset (x, y) and scale for width, height of SVGRoot."""

        width, w_unit = self.get_page_dimension('width')
        height, h_unit = self.get_page_dimension('height')
        vb_width, vb_height = self.check_viewbox(
            width, w_unit, height, h_unit)
        x_offset, y_offset, scale_x, scale_y = self.get_aspectratio_scale(
            width, w_unit, height, h_unit, vb_width, vb_height)

        # Treat '%' as special case, else apply scale to conversion
        # factor from __uuconv.  Use match_uuconv() to allow precision
        # tolerance of the document's page dimensions.
        w_scale, h_scale = [(scale if unit == '%' else
                             self.match_uuconv(self.__uuconv[unit] * scale))
                            for unit, scale in (
                                (w_unit, scale_x), (h_unit, scale_y))]

        return (x_offset, y_offset, w_scale, h_scale)

    def get_document_scale(self):
        """Return document scale factor.

        Calculate scale based on these SVGRoot attributes:
        'width', 'height', 'viewBox', 'preserveAspectRatio'
        """
        unitfactor = self.__uuconv['px']  # fallback
        width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
        return width_unitfactor or height_unitfactor or unitfactor

    # Unit conversion tools
    # =====================
    # Methods to assist unit handling in extensions / derived classes.

    def unittouu(self, string):
        """Return userunits given a string representation in units.

        Return float or None.
        """
        val = None
        if string is not None:
            val, unit = self.split_svg_length(string)
            if val is None:
                val = 0.0
            if unit is None:
                val /= self.get_document_scale()  # Assume default 'px'.
            elif unit in self.__uuconv.keys():
                val *= (self.__uuconv[unit] / self.get_document_scale())
        return val

    def uutounit(self, val, unit):
        """Return value in userunits converted to other units."""
        return val / (self.__uuconv[unit] / self.get_document_scale())

    def switch_uuconv(self, dpi):
        """Allow extensions to override internal resolution."""
        if dpi == '90':
            self.__uuconv = self.__uuconv_90dpi
        elif dpi == '96':
            self.__uuconv = self.__uuconv_96dpi
        else:  # unchanged
            pass

    #
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    # -----------------------------------------------------------------


class ClipBase(EffectCompat):
    """Effect class for setting, updating and releasing clips."""

    def __init__(self):
        """Init inkex.Effect class, parse default options."""
        EffectCompat.__init__(self)

        # global settings
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="verbose",
                                     default=True,
                                     help="Verbose mode")
        # tabs
        self.OptionParser.add_option("--tab",
                                     action="store",
                                     type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    def get_defs(self):
        """Return <defs> element.

        Create node if not already present in self.document.
        """
        defs = self.document.getroot().find(inkex.addNS('defs', 'svg'))
        if defs is None:
            defs = inkex.etree.SubElement(self.document.getroot(),
                                          inkex.addNS('defs', 'svg'))
        return defs

    def get_inkscape_version(self):
        """Return Inkscape version extracted from version attribute."""
        # NOTE: r14965 changed how this attribute is updated:
        # Inkscape no longer updates the attribute on load but on save.
        # New files thus don't have such an attribute anymore.
        # quick workaround: default to (assumed) 0.92
        inkscape_version_string = self.document.getroot().get(
            inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(
            r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    def new_layer(self, name='New Layer'):
        """Create new etree Element for layer with label *name*."""
        layer = inkex.etree.Element(inkex.addNS('g', 'svg'))
        layer.set('id', self.uniqueId("layer"))
        layer.set(inkex.addNS('groupmode', 'inkscape'), "layer")
        layer.set(inkex.addNS('label', 'inkscape'), name)
        return layer

    def create_layer(self, name='Layer'):
        """Append custom layer with label *name* to self.document."""
        layer = self.new_layer(name)
        self.document.getroot().append(layer)
        return layer

    def append_layer(self, name='Layer'):
        """Append custom layer with label *name* to self.document."""
        layer = self.new_layer(name)
        self.document.getroot().append(layer)
        return layer

    def insert_layer(self, name='Layer', pos=None):
        """Insert layer with label *name* at *pos* in self.document."""
        if pos is None:  # append
            pos = len(self.document.getroot())
        layer = self.new_layer(name)
        self.document.getroot().insert(pos, layer)
        return layer

    def showme(self, msg):
        """Verbose output."""
        if self.options.verbose:
            inkex.debug(msg)

    def get_clip_path(self, node):
        """Return clipPath element."""
        match = re.search(r'url\(\#(.*)\)', node.get('clip-path', ""))
        if match is not None:
            linked_node = self.getElementById(match.group(1))
            if linked_node is not None:
                if is_clip_path(linked_node):
                    return linked_node

    def get_clip_def(self, node, parents=False):
        """Return clip-path from node or node's ancestor(s)."""
        if is_clipped(node):
            return (node, self.get_clip_path(node))
        elif parents:
            if node.getparent() is not None:
                return self.get_clip_def(node.getparent(), parents)
        return (node, None)

    def clip_release(self, node, keep=True):
        """Release clip applied to node."""
        clipped_node, clip_path_def = self.get_clip_def(node)
        if clip_path_def is not None:
            if keep:
                index = clipped_node.getparent().index(node)
                for i, child in enumerate(clip_path_def, start=index+1):
                    node.getparent().insert(i, child)
                    mat.apply_copy_from(clipped_node, child)
            clip_path_def.getparent().remove(clip_path_def)
            clipped_node.set('clip-path', "none")

    def clip_set(self, node, path):
        """Clip node with path.

        First, add clipPath definition with path to <defs>.
        Then clip node with new clipPath.
        """
        clip = inkex.etree.SubElement(self.get_defs(),
                                      inkex.addNS('clipPath', 'svg'))
        clip.append(path)
        clip_id = self.uniqueId('clipPath')
        clip.set('id', clip_id)
        node.set('clip-path', 'url(#{0})'.format(clip_id))

    def clip_wrap(self, img_node, path):
        """Wrap <image> node in group and clip group with path."""
        group = wrap_group(img_node)
        if group is not None:
            mat.apply_to_d(mat.absolute_diff(img_node, path), path)
            self.clip_set(group, path)

    def clip_action(self):
        """Core method defined in each clip-modifying extension."""
        raise NotImplementedError

    def effect(self):
        """Initiate extension-side actions for current class instance."""
        self.clip_action()


class ClipObjects(ClipBase):
    """Effect class for clipping (selected) objects."""

    def __init__(self):
        """Init base class."""
        ClipBase.__init__(self)


class ClipLayers(ClipBase):
    """Effect class for clipping Inkscape layer(s)."""

    def __init__(self):
        """Init base class."""
        ClipBase.__init__(self)


class ClipPage(ClipBase):
    """Effect class for clipping to page area."""

    def __init__(self):
        """Init base class."""
        ClipBase.__init__(self)

        # instance attributes
        self.objects = []
        self.pagerect = None
        self.pagerect_id = 'rectPage'
        self.clip_baseid = 'clipPage_'

    def page_geometry(self):
        """Return bbox of page area based on viewBox."""
        viewbox = self.get_viewbox()
        if viewbox is not None:
            vb_x, vb_y = viewbox[:2]
            vb_width, vb_height = viewbox[2:]
        else:
            vb_x = vb_y = 0.0
            width, w_unit = self.get_page_dimension('width')
            height, h_unit = self.get_page_dimension('height')
            vb_width, vb_height = self.check_viewbox(
                width, w_unit, height, h_unit)
        return [vb_x, vb_y, vb_width, vb_height]

    def update_page_rect(self, page_geom):
        """Update page rect to current page geometry."""
        if self.pagerect is not None and is_rect(self.pagerect):
            self.pagerect.set('x', str(page_geom[0]))
            self.pagerect.set('y', str(page_geom[1]))
            self.pagerect.set('width', str(page_geom[2]))
            self.pagerect.set('height', str(page_geom[3]))

    def create_page_rect(self):
        """Create XML node for page rect."""
        rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
        rect.set('id', self.pagerect_id)
        self.get_defs().append(rect)
        self.pagerect = rect
        self.update_page_rect(self.page_geometry())

    def create_page_rect_clone(self):
        """Create <use> instance of XML node for page rect."""
        if self.pagerect is None:
            self.create_page_rect()
        clone = inkex.etree.Element(inkex.addNS('use', 'svg'))
        clone.set(inkex.addNS('href', 'xlink'),
                  '#{0}'.format(self.pagerect_id))
        return clone

    def add_pageclip_def(self, clipped_id=None):
        """Create clipPath element for page area."""
        if clipped_id is None:
            clip_id = self.uniqueId(self.clip_baseid)
        else:
            clip_id = '{0}{1}'.format(self.clip_baseid, clipped_id)
        clip_def = inkex.etree.Element(inkex.addNS('clipPath', 'svg'))
        clip_def.append(self.create_page_rect_clone())
        clip_def.set('id', clip_id)
        self.get_defs().append(clip_def)
        return clip_id

    def get_pageclip_def(self, node):
        """Return clipPath element for page area."""
        _, clip_def = self.get_clip_def(node, parents=False)
        if clip_def.get('id').startswith(self.clip_baseid):
            return clip_def
        else:
            return None

    def pageclip_transform(self, node):
        """Adjust transform of page rect clone in clipPath."""
        clip_def = self.get_pageclip_def(node)
        if clip_def is not None and 'transform' in node.attrib:
            cmat = simpletransform.invertTransform(
                simpletransform.parseTransform(node.get('transform')))
            clip_def[0].set('transform', simpletransform.formatTransform(cmat))


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
