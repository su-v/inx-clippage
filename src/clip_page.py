#!/usr/bin/env python
"""
clip_page - clip Inkscape top-level layers to page area

Copyright 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Specification:

    1) Clip top-level layers to page area
        - resolve document scale (viewBox, width, height, preserveAspectRatio)
        - add root_page_rect to defs
        - add clip-path to each top-level layer
          optionally apply to all top-level elements or
          optionally wrap content in one top-level layer first
    2) Update page clip to current page area
        - resolve document scale (viewBox, width, height, preserveAspectRatio)
        - if changed: update dimension and position of root_page_rect
    3) Release clip(s) to page area
        - check each top-level layer for clip to root_page_rect
        - remove clip-path attribute
        - if not referenced by any clips:
          delete root_page_rect in defs

"""
# local library
import inkex
import clip_lib.common as modclip


class PageClip(modclip.ClipPage):
    """Effect-based class to clip top-level layers to page area."""

    def __init__(self):
        """Init base class and parse options for PageClip class."""
        modclip.ClipPage.__init__(self)

        # clip_page options
        self.OptionParser.add_option("--action",
                                     action="store",
                                     type="string",
                                     dest="action",
                                     default="set",
                                     help="Clip action to apply")
        self.OptionParser.add_option("--layers_only",
                                     action="store",
                                     type="inkbool",
                                     dest="layers_only",
                                     default=True,
                                     help="Clip top-level layers only")

    def get_objects(self):
        """Get list of objects to be clipped to page area."""
        self.objects = []
        if self.options.layers_only:
            self.objects = list(modclip.layer_objs(self.document))
        else:
            for child in self.document.getroot().iterchildren():
                if child.tag not in modclip.IGNORE_ELEMENTS:
                    self.objects.append(child)

    def pageclip_set(self):
        """Clip layers / objects in SVGRoot to page area."""
        for node in self.objects:
            if ('clip-path' not in node.attrib or not
                    node.get('clip-path').startswith(
                        'url(#{0}'.format(self.clip_baseid))):
                node.set('clip-path',
                         'url(#{0})'.format(
                             self.add_pageclip_def(node.get('id'))))
                self.pageclip_transform(node)

    def pageclip_update(self):
        """Update page clip to current size of page area."""
        if self.pagerect is None:
            self.create_page_rect()
        else:
            self.update_page_rect(self.page_geometry())
        for node in self.objects:
            self.pageclip_transform(node)

    def pageclip_release(self):
        """Release all clippings to page area."""
        for node in self.objects:
            if ('clip-path' in node.attrib and
                    node.get('clip-path').startswith(
                        'url(#{0}'.format(self.clip_baseid))):
                self.clip_release(node, keep=False)

    def clip_action(self):
        """Main dispatcher for pageclip effect."""
        self.get_objects()
        self.pagerect = self.getElementById(self.pagerect_id)
        if self.options.action == 'set':
            self.pageclip_set()
        elif self.options.action == 'update':
            self.pageclip_update()
        elif self.options.action == 'release':
            self.pageclip_release()
        elif self.options.action == 'page_bbox':
            inkex.debug(self.page_geometry())
        else:  # unknown action
            pass


if __name__ == '__main__':
    ME = PageClip()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
